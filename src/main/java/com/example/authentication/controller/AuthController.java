package com.example.authentication.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.authentication.dto.LoginDTO;
import com.example.authentication.dto.UserDTO;
import com.example.authentication.interfaces.AuthService;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
@RequestMapping("/auth")
public class AuthController {
	@Autowired
	private AuthService authService;

	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Boolean> logIn(@RequestBody LoginDTO login) {
		Boolean response = this.authService.login(login);
		return new ResponseEntity<Boolean>(response, HttpStatus.ACCEPTED);
	}
	
	@PostMapping(value = "/add-user", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<UserDTO> addUser(@RequestBody UserDTO dto){
		UserDTO response = this.authService.addUser(dto);
		return new ResponseEntity<UserDTO> (response, HttpStatus.CREATED);
	}
	
	@GetMapping (value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<UserDTO> getUser(@PathVariable ("id") Integer id){
		UserDTO response = this.authService.getUser(id);
		return new ResponseEntity<UserDTO> (response , HttpStatus.OK);
	}
	
	@PutMapping (value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Boolean> updateUser(@PathVariable("id") Integer id, @RequestBody UserDTO dto){
		Boolean response = this.authService.updateUser(id, dto);
		return new ResponseEntity<Boolean>(response, HttpStatus.OK);
	}
	
	@DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Boolean> deleteUser(@PathVariable("id") Integer id){
		Boolean response = this.authService.deleteUser(id);
		return new ResponseEntity<Boolean>(response, HttpStatus.OK);
	}
	
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<UserDTO>> getAllUser(){
		List<UserDTO> response = this.authService.getAllUser();
		return new ResponseEntity<List<UserDTO>>(response, HttpStatus.OK);
	}
	
	@GetMapping(value = "/login", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<LoginDTO>> getLogin(){
		List<LoginDTO> response = this.authService.getLogin();
		return new ResponseEntity<List<LoginDTO>>(response ,HttpStatus.OK);
	}
	
}
