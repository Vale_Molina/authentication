package com.example.authentication.entities;

import org.springframework.data.annotation.Id;

import com.example.authentication.dto.LoginDTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {
	@Id
	private Integer id;
	private String name;
	private String email;
	private LoginDTO login;
}
