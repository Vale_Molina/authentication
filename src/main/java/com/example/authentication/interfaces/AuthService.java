package com.example.authentication.interfaces;

import java.util.List;

import com.example.authentication.dto.LoginDTO;
import com.example.authentication.dto.UserDTO;

public interface AuthService {
	Boolean login(LoginDTO login);
	
	UserDTO addUser(UserDTO user);

	UserDTO getUser(Integer id);

	Boolean updateUser(Integer id, UserDTO dto);

	Boolean deleteUser(Integer id);

	List<UserDTO> getAllUser();

	List<LoginDTO> getLogin();
}
