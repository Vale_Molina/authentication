package com.example.authentication.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.example.authentication.dto.LoginDTO;
import com.example.authentication.entities.User;

public interface UserRepository extends MongoRepository<User, Integer> {
	User findByLogin(LoginDTO login);
}
