package com.example.authentication.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.authentication.dto.LoginDTO;
import com.example.authentication.dto.UserDTO;
import com.example.authentication.entities.User;
import com.example.authentication.interfaces.AuthService;
import com.example.authentication.repository.UserRepository;

@Service
public class AuthServiceImpl implements AuthService {

	@Autowired
	private UserRepository userRepository;

	@Override
	public Boolean login(LoginDTO login) {

		User entity = this.userRepository.findByLogin(login);

		UserDTO dto = this.getUserDTO(entity);
		 if(dto != null) {
			 return true;
		 }
		 return false;
	}
	
	@Override
	public UserDTO getUser(Integer id) {
		Optional<User> optionalUser = this.userRepository.findById(id);
		if (optionalUser.isPresent()) {
			User entity = optionalUser.get();
			UserDTO response = this.getUserDTO(entity);
			return response;
		}
		return null;
	}
	
	private UserDTO getUserDTO(User entity) {
 
		if(entity == null) {
			
			return null;
		}
		UserDTO response = new UserDTO();
		response.setEmail(entity.getEmail());
		response.setName(entity.getName());
		response.setId(entity.getId());
		response.setLogin(entity.getLogin());

		return response;
	}

	@Override
	public UserDTO addUser(UserDTO user) {
		User entity = this.getUserEntity(user);
		User userEntity = this.userRepository.insert(entity);
		if ( userEntity != null) {
			return this.getUserDTO(userEntity );
		}
	
		return null;
	}
	
	private User getUserEntity(UserDTO user) {
		User response = new User();
		if(user == null) {
			return null;
		}
		response.setEmail(user.getEmail());
		response.setName(user.getName());
		response.setId(user.getId());
		response.setLogin(user.getLogin());
		return response;
	}

	@Override
	public Boolean updateUser(Integer id, UserDTO user) {
		Optional<User> optionalUser = this.userRepository.findById(id);
		if(optionalUser.isPresent()) {
			User entity = getUpdateUserEntity(user, id);
			this.userRepository.save(entity);
			return true;
		}
		return false;
		
		
	}

	private User getUpdateUserEntity(UserDTO user, Integer id) {
		User response = new User();

		response.setEmail(user.getEmail());
		response.setName(user.getName());
		response.setId(id);
		response.setLogin(user.getLogin());
		return response;
	
	}

	@Override
	public Boolean deleteUser(Integer id) {
		Optional<User> optionalUser = this.userRepository.findById(id);
		if(optionalUser.isPresent()) {
			User entity = optionalUser.get();
			this.userRepository.delete(entity);
			return true;
		}
		return false;
	}

	@Override
	public List<UserDTO> getAllUser() {
		List<User> listUser = this.userRepository.findAll();
		if(listUser != null) {
			List<UserDTO> response = new ArrayList<UserDTO>();
			for(User item : listUser) {
				response.add(this.getAllUserDTO(item));
			}
			return response;
		}
		return null;
		
	}

	private UserDTO getAllUserDTO(User item) {
		UserDTO response = new UserDTO();
		response.setEmail(item.getEmail());
		response.setName(item.getName());
		response.setId(item.getId());
		response.setLogin(item.getLogin());
		return response;
	}

	@Override
	public List<LoginDTO> getLogin() {
		List<User> listUser = this.userRepository.findAll();
		List<LoginDTO> response = new ArrayList<LoginDTO>();
		if(listUser != null) {
			for(User item : listUser) {
				response.add(this.getAllLogin(item));
			}
			return response;
		}
		return null;
	}

	private LoginDTO getAllLogin(User item) {
		LoginDTO response = new LoginDTO();
		response.setUserName(item.getLogin().getUserName());
		response.setPassword(item.getLogin().getPassword());
		return response;
	}
	

}
